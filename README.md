## Jaguar Design Studio Web Developer Exercise

### Introduction

The following exercises are to be completed by web developer candidates applying for a position at Jaguar Design Studio. You will need to use Git and a BitBucket account to complete and submit the exercises.

It is expected that some applicants will have no previous experience with Git and/or BitBucket. Successfully using these tools is part of the exercise.

Follow the instructions that were provided in the job posting for details on how to complete this exercise.

### Exercise: Ruby

Candidates are not necessarily expected to be familiar with the Ruby language, but the following exercises are intended to be simple enough that a qualified programmer should be able to complete them after spending a couple of hours learning basic Ruby syntax, and a little consulting of the Ruby language docs.

To complete this exercise, implement the incomplete methods defined in "exercise.rb", such that they pass the tests defined in "exercise_spec.rb".

In order to run the tests, you will need Minitest installed (if you're running Ruby 1.9, Minitest is already installed - otherwise, install it from Rubygems by running "gem install minitest").

To execute the test suite, run:

    $ ruby exercise_spec.rb

The Exercise class is correctly implemented once the test suite returns:

    3 tests, 8 assertions, 0 failures, 0 errors, 0 skips

### Exercise: JavaScript

To complete this exercise, implement the incomplete methods defined in "exercise.js", such that they pass the tests defined in "exercise_spec.js".

In order to run the tests, open the file "SpecRunner.html" in your browser.

The Exercise object is correctly implemented once the test suite returns:

    2 specs, 0 failures
