describe("Exercise", function() {
  var exercise;

  beforeEach(function() {
    exercise = new Exercise();
  });

  it("remove_odds_from_array(): should remove odd numbers", function() {
    var test_array_1 = [2, 6, 9, 5, 7, 4, 7, 6, 8, 0, 2, 1];
    var test_array_2 = [5, 17, 109, 54, 17, 22, 32, 67, 80, 142, 1771, 76, 0, 10];
    expect(exercise.remove_odds_from_array(test_array_1)).toEqual([2,6,4,6,8,0,2]);
    expect(exercise.remove_odds_from_array(test_array_2)).toEqual([54,22,32,80,142,76,0,10]);
  });

  it("convert_fahrenheit_and_celsius(): should convert between fahrenheit and celsius", function() {
    expect(exercise.convert_fahrenheit_and_celsius(32, "C")).toEqual("89.60 F");
    expect(exercise.convert_fahrenheit_and_celsius(27, "F")).toEqual("-2.78 C");
  });
});
