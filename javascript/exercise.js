/* exercise.js */

var Exercise = function() {
  return this;
};

/*
  * Implement a function that, when given an array of integers,
  * returns the array with all odd numbers removed
  */
Exercise.prototype.remove_odds_from_array = function(arr) {
  /* TODO: Implement this function */
}; 

/*
 * Implement a function that converts between fahrenheit and celsius
 *
 * Function arguments:
 *   value: an integer temperature value to convert from
 *   type: a character indicating the format (fahrenheit or celsius) of argument "value"
 *
 * For argument "type", the function should be able to accept values of "C" and "F".
 *
 * The return value should be a string containing the converted value, rounded to 2 decimal places,
 * along with a space and the letter type of the converted value (eg. "80.23 F")
 */
Exercise.prototype.convert_fahrenheit_and_celsius = function(value, type) {
  /* TODO: Implement this function */
};
