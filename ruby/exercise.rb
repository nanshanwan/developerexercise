# exercise.rb

class Exercise

  # Implement this method such that it returns the sum of
  # an array of integers
  def self.sum_array(arr)
    # TODO: implement this method
  end

  # For processing credit card payments, Paypal adds a fee
  # of 2.9% of the charged value, plus an additional $0.30 
  # flat fee in addition to that.
  #
  # Define the following method so that it takes the subtotal,
  # and returns a total with these fees applied.
  #
  # Round up to the nearest cent.
  # 
  def self.paypal_credit_card_fee(subtotal)
    # TODO: implement this method
  end

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # TODO: implement this method
  end

end
