require 'minitest/spec'
require 'minitest/autorun'
require './exercise'

describe Exercise do
  it "should sum an array with Exercise#sum_array" do
    fibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    wheel_of_fortune = [?R.ord, ?S.ord, ?T.ord, ?L.ord, ?N.ord, ?E.ord]
    lost = [4, 8, 15, 16, 23, 42]
    Exercise.sum_array(fibonacci).must_equal 232
    Exercise.sum_array(wheel_of_fortune).must_equal 472
    Exercise.sum_array(lost).must_equal 108
  end

  it "should return a credit card charge value with Paypal fees added" do
    Exercise.paypal_credit_card_fee(10).must_equal 10.59
    Exercise.paypal_credit_card_fee(4.99).must_equal 5.43
    Exercise.paypal_credit_card_fee(17.49).must_equal 18.30
  end

  it "should return a marklar'd string" do
    Exercise.marklar("The quick brown fox").must_equal "The marklar marklar fox"
    Exercise.marklar("Down goes Frazier").must_equal "Down goes Marklar"
  end

end
